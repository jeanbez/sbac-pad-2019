# Detecting I/O Access Patterns of HPC Workloads at Runtime

Jean Luca Bez<sup>1</sup>, Francieli Zanon Boito<sup>2</sup>, Ramon Nou<sup>3</sup>, Alberto Miranda<sup>3</sup>, Toni Cortes<sup>3,4</sup>, and Philippe O. A. Navaux<sup>1</sup>

> <sup>1</sup> Institute of Informatics, Federal University of Rio Grande do Sul (UFRGS), Porto Alegre, Brazil
>
> <sup>2</sup> Univ. Grenoble Alpes, Inria, CNRS, Grenoble INP, LIG, 38000 Grenoble, France
>
> <sup>3</sup> Barcelona Supercomputing Center, Spain
>
> <sup>4</sup> Universitat Politécnica de Catalunya, Spain

This repository contains the datasets, source-codes, and parsing scripts from our paper: 

https://jeanbez.gitlab.io/sbac-pad-2019/

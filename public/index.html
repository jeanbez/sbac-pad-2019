<!DOCTYPE html>
<html lang="pt">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Detecting I/O Access Patterns of HPC Workloads at Runtime</title>

		<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:400,400i|IBM+Plex+Sans+Condensed:400,400i|IBM+Plex+Sans:100,100i,400,400i,700,700i|IBM+Plex+Serif:400,400i" rel="stylesheet">
        <link rel="stylesheet" href="css/default.css"/>
    </head>
    <body>

    	<p>
			<b class="label-black">COMPANION MATERIALS</b>
			<em class="conference">SBAC-PAD 2019</em>
		</p>

		<h1>Detecting I/O Access Patterns of HPC Workloads at Runtime</h1>

		<p>
			<em>Jean Luca Bez<sup>1</sup>, Francieli Zanon Boito<sup>2</sup>, Ramon Nou <sup>3</sup>, Alberto Miranda <sup>3</sup>, Toni Cortes <sup>4</sup>, and Philippe O. A. Navaux<sup>1</sup></em>
		</p>

		<p class="institutions">
			<sup>1</sup> Institute of Informatics, Federal University of Rio Grande do Sul (UFRGS), Porto Alegre, Brazil<br/>
			<sup>2</sup> Univ. Grenoble Alpes, Inria, CNRS, Grenoble INP, LIG, 38000 Grenoble, France<br/>
			<sup>3</sup> Barcelona Supercomputing Center, Spain<br/>
			<sup>4</sup> Universitat Politécnica de Catalunya, Spain
		</p>

		<h2>Contents</h2>

		<p>
			This repository contains the datasets, source-codes, and parsing scripts from our paper:
		</p>
		<p class="url">
			<a href="https://gitlab.com/jeanbez/sbac-pad-2019/">https://gitlab.com/jeanbez/sbac-pad-2019/</a>
		</p>
		<p>
			Please, feel free to use it to reproduce our research or to build new contributions.
		</p>

		<h2>Dataset</h2>

		<p class="information">
			To cover the most common I/O access pattern of HPC applications, we used the MPI-IO Test benchmark too, to issue requests using the MPI-IO library. We varied the number of processes (128, 256, or 512), operation (read or write), file layout (shared-file or file-per-process), spatiality (contiguous or 1D-strided), and request sizes (32 or 256KB &ndash; smaller than the stripe size or larger enough so that all servers are accessed).  For each experiment, a total of $4$~GB of data was accessed. We also deployed a different number of intermediate I/O nodes (1, 2, 4, or 8). In total, 144 different scenarios (we do not test the file-per-process 1D-strided combinations as this access pattern is not usual) were considered. The full set was executed in random order to minimize unforeseen impacts.
		</p>

		<table>
			<tr>
				<th colspan="5"><strong>Observations</strong></th>
			</tr>
			<tr>
				<td class="left"><strong>Read</strong></td>
				<td class="numeric">29,929</td>
				<td class="versus"><em>vs.</em></td>
				<td class="numeric text-right">100,406</td>
				<td class="right"><strong>Write</strong></td>
			</tr>
			<tr>
				<td class="left"><strong>Shared-file</strong></td>
				<td class="numeric">72,802</td>
				<td class="versus"><em>vs.</em></td>
				<td class="numeric text-right">57,533</td>
				<td class="right"><strong>File-per-process</strong></td>
			</tr>
			<tr>
				<td class="left"><strong>Contiguous</strong></td>
				<td class="numeric">94,997</td>
				<td class="versus"><em>vs.</em></td>
				<td class="numeric text-right">35,338</td>
				<td class="right"><strong>1D-strided</strong></td>
			</tr>
		</table>

		<p class="information">
			Since the patterns have a fixed duration, the number of observations is <em>not</em> the same for each benchmark, as it depends on the execution time. These metrics compose a data set of over one million observations. The number of patterns representing different benchmark parameters is detailed by the table.
		</p>

		<ul>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/datasets/metrics/access-pattern-metrics.csv"><b class="label-red">DATASET</b> Access pattern dataset</a> 
			</li>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/datasets/metrics/access-pattern-metrics-full.csv"><b class="label-red">DATASET</b> Complete dataset with the collected metrics</a> 
			</li>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/datasets/metrics/access-pattern-metrics-validation.csv"><b class="label-red">DATASET</b> Validation dataset with the collected metrics</a> 
			</li>
		</ul>

		<h2>Decision Tree</h2>

		<p class="information">
			Decision trees are often an efficient approach to classification problems. Therefore they might prove suitable for detecting the I/O access pattern at runtime. To build our tree, we applied the C5.0 algorithm which is a data mining tool for discovering patterns that delineate categories, assembling them into classifiers, and using them to make predictions. The classifiers are expressed as decision trees or sets of if-then rules, that are generally easy to understand and implement.
		</p>

		<p class="diagram">
			<img src="images/decision-tree-model.svg" alt="Decision Tree Model" height="400px">
		</p>

		<ul>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/models/decision-tree/decision-tree-model.rda"><b class="label-light">MODEL</b> Trained decision tree model in R</a> 
			</li>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/datasets/decision-tree/decision-tree-detected.csv"><b class="label-light">DATASET</b></a> Dataset with the collected metrics and decision tree detection results
			</li>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/datasets/decision-tree/decision-tree-training-runtimes.csv"><b class="label-light">DATASET</b></a> Dataset with the runtime for training
			</li>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/datasets/decision-tree/decision-tree-predict-runtimes.csv"><b class="label-light">DATASET</b></a> Dataset with the runtime to predict the class
			</li>
		</ul>

		<h2>Random Forests</h2>

		<p class="information">
			Random forests are an ensemble method which makes predictions by averaging over the predictions of several independent decision trees. Such approach often demonstrates improvements in classification accuracy due to the ensemble of trees and the voting for the most popular class.
		</p>

		<ul>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/models/random-forest/random-forest-model.rda"><b class="label-purple">MODEL</b> Trained random forest model in R</a> 
			</li>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/datasets/random-forest/random-forest-detected.csv"><b class="label-purple">DATASET</b></a> Dataset with the collected metrics and random forest detection results
			</li>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/datasets/random-forest/random-forest-training-runtimes.csv"><b class="label-purple">DATASET</b></a> Dataset with the runtime for training
			</li>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/datasets/random-forest/random-forest-predict-runtimes.csv"><b class="label-purple">DATASET</b></a> Dataset with the runtime to predict the class
			</li>
		</ul>

		<h2>Neural Network</h2>

		<p class="information">
			Our model consists of three layers: an input layer with the five features, a hidden layer with the same number of neurons, and an output layer with three units, one for each class. The first two layers use a Rectified Linear Unit (ReLU) activation function with a normal kernel initialization function. The output layer uses <em>softmax</em>. We used the <em>RMSProp</em> optimizer with learning rate of 0.001 and a momentum of 0.9. The loss function was the categorical cross-entropy.
		</p>

		<p class="diagram">
			<img src="images/neural-network-model.png" alt="Neural Network Model" height="250px">
		</p>

		<ul>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/models/neural-network/neural-network.hdf5"><b class="label-yellow">MODEL</b> Trained neural network model in R</a> 
			</li>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/datasets/neural-network/neural-network-detected.csv"><b class="label-yellow">DATASET</b></a> Dataset with the collected metrics and neural network detection results
			</li>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/datasets/neural-network/neural-network-training-runtimes.csv"><b class="label-yellow">DATASET</b></a> Dataset with the runtime for training
			</li>
			<li>
				<a href="https://gitlab.com/jeanbez/sbac-pad-2019/blob/master/datasets/neural-network/neural-network-predict-runtimes.csv"><b class="label-yellow">DATASET</b></a> Dataset with the runtime to predict the class
			</li>
		</ul>

		<h3>Acknowledgement</h3>

		<p class="acknowledgement">
			This study was financed in part by the Coordenação de Aperfeiçoamento de Pessoal de Nível Superior - Brasil (CAPES) - Finance Code 001. It has also received support from the Conselho Nacional de Desenvolvimento Científico e Tecnológico (CNPq), Brazil; the LICIA International Laboratory; NCSA-Inria-ANL-BSC-JSC-Riken Joint-Laboratory on Extreme Scale Computing (JLESC); the Spanish Ministry of Science and Innovation under the TIN2015&mdash;5316 grant; and the Generalitat de Catalunya under contract 2014&mdash;SGR&mdash;1051. This research received funding from the European Union's Horizon 2020 research and innovation programme under the Marie Sk&#322;odowska-Curie grant agreement No. 800144. Experiments presented in this paper were carried out using the Grid'5000 testbed, supported by a scientific interest group hosted by Inria and including CNRS, RENATER and several Universities as well as other organizations (www.grid5000.fr).
		</p>

		<p class="copyright">
			&copy; 2019 Jean Luca Bez &mdash; Federal University of Rio Grande do Sul, Brazil
		</p>

    </body>
</html>
